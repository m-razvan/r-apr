package main.java

import org.apache.spark.sql.{DataFrame, Row, functions}

/**
  * Created by dan on 25.03.2017.
  */
object HighDensityInstanceExpander {

  /**
    * Computes a distance based value between two rows
    *
    * @param rowOne the first row (representing a point in n-dimensional space)
    * @param rowTwo the second row (representing a point in n-dimensional space)
    * @param relevantFeatures the relevant features which will be used in computing the score
    * @param eps constant used to offset the distance between two points which are very close together
    * @return a score based on the distance of two points; the closer the points the higher the score is,
    *         otherwise it becomes lower
    */
  def computeValue(rowOne : Row,
                   rowTwo : Row,
                   relevantFeatures : Array[String],
                   eps : Double) : Double = {

    // Compute Euclidean Distance
    val distance = relevantFeatures.map[Double, Array[Double]](feature => {

      Math.pow(rowOne.getAs[Double](feature) - rowTwo.getAs[Double](feature), 2)

    }).sum

    // Compute value of this distance and return it
    1.0 / (eps + distance)
  }

  /**
    * Determines the highest density point for a given bag, based on the distance to the other instances in the bag
    *
    * @param df the DF containing the bag
    * @param relevantFeatures the relevant features
    * @return a DF, containing a singular row (the highest density row)
    */
  def highestDensityBagInstance(df : DataFrame,
                                relevantFeatures : Array[String]) : DataFrame = {

    // Transform the DF to Array[Row]; seems impossible to do it Distributed
    val rows = df.collect()

    // Find the instance with the greatest value
    var index : Int = 0
    var currentIndex : Int = 0
    var value : Double = Double.MinValue

    rows.foreach(row => {

      // Because this calculates the distance between a row with itself, the result will be displaced
      // by (1 / eps); however, since this happens to every row, it shouldn't influence the result
      val result = df.rdd.map(innerRow => {
        // Compute the value(distance) for this pair of
        computeValue(
          row,
          innerRow,
          relevantFeatures,
          50.0)
      }).sum()

      // Determine if this row is better
      if (result > value) {

        value = result
        index = currentIndex
      }

      currentIndex = currentIndex + 1
    })

    val selectedRow = rows(index)

    // Return the best row as DF
    df.filter(row => {

      row.equals(selectedRow)

    })
  }

  /**
    * Expands an APR passed as parameter, based on the 'positive' bags, which are considered to have
    * a certain number of negative instances not yet included in the APR
    *
    * @param df the DF containing the positive bags
    * @param apr the APR whose boundaries need expanding
    * @param relevantFeatures the relevant features
    * @param bagIdColName the name of the column hosting the Bag Id
    * @return a new APR with its bounds expanded
    */
  def expandAPR(df : DataFrame,
                apr : Map[String, (Double, Double)],
                relevantFeatures : Array[String],
                bagIdColName : String) : Map[String, (Double, Double)] = {

    // Reassign to var so as to change it later
    var APR = apr

    // Get the ids of the bags in the DF
    val bagIds = df.select(df.col(bagIdColName)).distinct().collect()

    val spark = df.sparkSession

    // Will collect the highest density points
    var collector = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], df.schema)

    bagIds.foreach(x => {

      collector = collector.union(highestDensityBagInstance(df.where(df.col(bagIdColName).equalTo(x.getInt(0))), relevantFeatures))

    })

    //collector.show()

    // Get the bounds for each relevant feature as given by the density points
    val dfMax = collector.groupBy().max(relevantFeatures : _*).first()
    val dfMin = collector.groupBy().min(relevantFeatures : _*).first()

    // Expand the APR where needed
    for (feature <- apr) {

      var min = feature._2._1

      var max = feature._2._2

      if (dfMin.getAs[Double]("min(" + feature._1 + ")") < min)
        min = dfMin.getAs[Double]("min(" + feature._1 + ")")

      if (dfMax.getAs[Double]("max(" + feature._1 + ")") > max)
        max = dfMax.getAs[Double]("max(" + feature._1 + ")")

      // Update the APR
      APR += feature._1 -> (min, max)

    }

    //collector.show()

    // Return the updated APR
    APR
  }

}
