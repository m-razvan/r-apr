package main.java

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._

/**
  * Created by dan on 27.03.2017.
  */
/**
  * User aggregate function which reduces an entire DF representing multiple bags to a DF containing one row and one column
  * (when it's used after a groupBy, there will be two columns); The column will contain a Seq[Seq[Double]], each Seq[Double]
  * representing a bag instance, having the same order as the Strings in the relevantFeature array
  *
  * @param structTypeParam the schema of the input DF
  * @param relevantFeaturesParam the relevant features array
  */
class AggregateBag(structTypeParam: StructType, relevantFeaturesParam : Array[String]) extends UserDefinedAggregateFunction {

  private val relevantFeatures : Array[String] = relevantFeaturesParam

  // The input DF schema
  override def inputSchema: StructType = structTypeParam

  // Will manipulate this type throughout aggregation
  override def bufferSchema: StructType =
    StructType(Array[StructField](StructField("BEST", ArrayType(ArrayType(DoubleType)))))

  // Will return this in the agg column
  override def dataType: DataType = ArrayType(ArrayType(DoubleType))

  // Always
  override def deterministic: Boolean = true

  // Set the initial value of the buffer
  override def initialize(buffer: MutableAggregationBuffer): Unit = {

    // Initially the buffer will host an empty Seq of Seq's
    buffer(0) = Seq[Seq[Double]]()
  }

  // Natural update of the buffer
  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {

    buffer(0) = buffer(0).asInstanceOf[Seq[Seq[Double]]] :+
      relevantFeatures.map[Double, Seq[Double]](colName => input.getDouble(inputSchema.fieldIndex(colName)))
      /* input.toSeq.map[Double, Seq[Double]](_.asInstanceOf[Double]) */
  }

  // Merging two buffers together - saving in buffer1
  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    buffer1(0) = buffer1(0).asInstanceOf[Seq[Seq[Double]]] ++ buffer2(0).asInstanceOf[Seq[Seq[Double]]]
  }

  // Return the first element of the buffer as the result of aggregation, i.e. the Seq[Seq[Double]]
  override def evaluate(buffer: Row): Any = {
    buffer(0)
  }
}
