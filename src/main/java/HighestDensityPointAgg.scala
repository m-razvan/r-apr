package main.java

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._

/**
  * Created by dan on 27.03.2017.
  */
/**
  * Computes an intermediary APR from the most dense instances of the bags contained in the DF passed as constructor parameter;
  * The instances must meet a certain density lower bound, and must not be very far away from the APR bounds
  *
  * @param relevantFeaturesParam the relevant features vector
  * @param valueFunctionParam the function which computes the value of a point, in relation to another point; its values will be
  *                           summed in order to obtain the density
  * @param validationFunctionParam the function which will validate a particular instance as being viable (high density and close to the APR)
  */
class HighestDensityPointAgg(relevantFeaturesParam : Array[String],
                             valueFunctionParam : (Seq[Double], Seq[Double]) => Double,
                             validationFunctionParam : (Seq[Double], Double) => Boolean) extends UserDefinedAggregateFunction {

  private val relevantFeatures : Array[String] = relevantFeaturesParam
  private val valueFunction = valueFunctionParam
  private val validationFunction = validationFunctionParam


  // The input schema will be passed as constructor parameter
  override def inputSchema: StructType = StructType(Array[StructField](StructField("BEST", ArrayType(ArrayType(DoubleType)))))

  override def bufferSchema: StructType =
    StructType(
      Seq[StructField](
        StructField("MIN",
          ArrayType(DoubleType)
        ),
        StructField("MAX",
          ArrayType(DoubleType)
        )
      )
    )

/*
  // Will manipulate this during aggregation
  override def bufferSchema: StructType =
    StructType(
      Array[StructField](
        StructField("MIN_MAX",
          ArrayType(
            StructType(
              Seq(
                StructField("_1", DoubleType),
                StructField("_2", DoubleType)
              )
            )
          )
        )
      )
    )
*/

  // Will return this after aggregation is over
  override def dataType: DataType =
    ArrayType(
      StructType(
        Seq(
          StructField("_1", DoubleType),
          StructField("_2", DoubleType)
        )
      )
    )

  // Always
  override def deterministic: Boolean = true

  // Initialize all to (Max, Min)
  override def initialize(buffer: MutableAggregationBuffer): Unit = {

    // Will hold the minimal values of each relevant feature
    buffer(0) = relevantFeatures.indices.map(_ => Double.MaxValue)

    // Will hold the maximal feature of each relevant feature
    buffer(1) = relevantFeatures.indices.map(_ => Double.MinValue)

  }

  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {

    // Get the bag
    val bag : Seq[Seq[Double]] = input.getAs[Seq[Seq[Double]]](0)

    val f = (point: Seq[Double]) => {
      bag.map(p =>
        valueFunction(point, p)
      ).sum
    }

    var max = Double.MinValue

    val highDensityInstance = bag.maxBy(point => {

      val value = f(point)

      if (value > max)
        max = value

      value


      //f(point)
    })

    println("The max density is: " + max)

    if (validationFunction(highDensityInstance, max)) {

      buffer(0) = buffer(0).asInstanceOf[Seq[Double]].zip(highDensityInstance)
        .map(tuple => if (tuple._1 < tuple._2) tuple._1 else tuple._2)

      buffer(1) = buffer(1).asInstanceOf[Seq[Double]].zip(highDensityInstance)
        .map(tuple => if (tuple._1 > tuple._2) tuple._1 else tuple._2)

    }
  }

  // Merge function for two partial aggregate buffers
  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {

    // Get the Minimums
    buffer1(0) = buffer1(0).asInstanceOf[Seq[Double]].zip(buffer2(0).asInstanceOf[Seq[Double]])
      .map(tuple => if (tuple._1 < tuple._2) tuple._1 else tuple._2)

    // Get the Maximums
    buffer1(1) = buffer1(1).asInstanceOf[Seq[Double]].zip(buffer2(1).asInstanceOf[Seq[Double]])
      .map(tuple => if (tuple._1 > tuple._2) tuple._1 else tuple._2)

  }

  // Manipulates the data in buffer, and returns the final contents of the aggregate column
  // Return tuples between the corresponding pairs of min and max
  override def evaluate(buffer: Row): Any = {
    buffer(0).asInstanceOf[Seq[Double]].zip(buffer(1).asInstanceOf[Seq[Double]]).map[(Double, Double), Seq[(Double, Double)]](x => x)
  }
}
