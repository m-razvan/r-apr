package main.java


import java.io.{File, PrintWriter}

import org.apache.avro.generic.GenericData.StringType
import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.ml.{Pipeline, PipelineStage}
import org.apache.spark.sql.types.{DoubleType, IntegerType, LongType, NumericType}
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

import scala.io.Source


/**
  * Created by dan on 19.03.2017.
  */
object RAPR {

  def main(args : Array[String]) : Unit = {

    if (args.length != 5) {

      println("Too few Arguments!\n" +
        "Usage: R-APR-1.0-SNAPSHOT-jar-with-dependencies.jar " +
        "POSITIVE_CSV_NAME NEGATIVE_CSV_NAME OUTPUT_FILE BAG_ID_COLUMN_NAME PATH_TO_REL_FEATURES <RETURN>")

      System.exit(0xFF)
    }

    // Read the path from the command line
    //val path = args(0)

    val spark = SparkSession
      .builder()
      .appName("SparkSessionZipsExample")
      .getOrCreate()

    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.FATAL)

    // The two DataFrames must have the same headers!
    val dffPositives = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema",  "true")
      .csv(args(0))

    // The two DataFrames must have the same headers!
    val dffNegatives = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema",  "true")
      .csv(args(1))

    // Read the relevant features from a file

    val relevantFeatures : Array[String] = parseRelevantFeaturesFile(args(4))

    val apr = GenerateRAPRClassifier(dffPositives,
      dffNegatives,
      spark,
      args(3),
      relevantFeatures)

    // Display the APR
    displayAPR(apr)

    // Write the APR
    writeAPRToFile(apr, args(2))

  }

  /**
    * USE THIS: Generate an RAPR Classifier relative to the positive and negative DF, and the excluded features.
    * The positiveDF and the negativeDF must both have the same headers.
    *
    * @param positiveDF the positive bags DataFrame
    * @param negativeDF the negative bags DataFrame
    * @param spark the SparkSession
    * @param bagIdColumnName the column name where the bag id is hosted
    * @param relevantFeatures the initial relevant features
    * @return an APR classifier
    */
  def GenerateRAPRClassifier(positiveDF : DataFrame,
                            negativeDF : DataFrame,
                            spark : SparkSession,
                            bagIdColumnName : String,
                            relevantFeatures : Array[String]) : Map[String, (Double, Double)] = {

    // Format the DataFrames so as to make them compatible with the APR Algorithm
    val dfPositives = transformData(positiveDF, bagIdColumnName, relevantFeatures)
    val dfNegatives = transformData(negativeDF, bagIdColumnName, relevantFeatures)

    dfPositives.printSchema()
    dfPositives.show()

    dfNegatives.printSchema()
    dfNegatives.show()

    //System.exit(10)

    // Prepare the feature vectors which control the do-while loop
    var oldRelevantFeatures = relevantFeatures
    var newRelevantFeatures = relevantFeatures

    // Declare the APR object
    var APR : Map[String, (Double, Double)] = Map[String, (Double, Double)]()

    // Declare the Phase Objects
    // Phase 1 - Greedy and Backfit (Growth stage)
    val allNegativeExpand = AllNegativeExpand

    // Phase 2 - Feature Selection (Discrimination stage)
    val featureSelection = Select

    // Phase 3 - Margin Expand (Expand stage)
    val marginExpand = MarginExpand

    // Start the algorithm
    // Phase 1 and 2 Loop until relevant features converge
    do {
      // Get the most relevant features
      oldRelevantFeatures = newRelevantFeatures

      // Generate an Initial APR based on the negative bags
      APR = allNegativeExpand.createAPR(dfNegatives, dfPositives, oldRelevantFeatures, bagIdColumnName)

      /*
      for (dimensions <- APR)
        println("Feature " + dimensions._1 + " bounds: [" + dimensions._2._1 + ", " + dimensions._2._2 + "]")
      */

      // Get the relevant features based on the negative bags
      newRelevantFeatures = featureSelection.featureSelection(dfPositives, APR, oldRelevantFeatures, 0.05, bagIdColumnName, spark)

    } while(!oldRelevantFeatures.sameElements(newRelevantFeatures))

    // Phase 3 - Expand the APR bounds
    APR = marginExpand.expandMargins(APR, dfNegatives)

    //APR is done -> return it
    APR
  }

  /**
    * USE THIS: Transforms a DF passed as parameter to something that suits the APR algorithm
    *
    * @param initialDf the DF which will be transformed
    * @param bagIdColumn the name of the column hosting the BagId
    * @param relevantColumns the relevant features which will be kept and transformed (if need be)
    * @return
    */
  def transformData(initialDf : DataFrame,
                    bagIdColumn : String,
                    relevantColumns : Array[String]) : DataFrame = {

    // Cast to var so as to allow manipulation
    var df = initialDf

    val exceptionColumns = bagIdColumn +: relevantColumns

    // Eliminate the irrelevant columns
    for (columnName <- df.schema.fields
         if !exceptionColumns.contains(columnName.name))
      df = df.drop(columnName.name)

    for (column <- df.schema.fields) {

      // If column type is Integer or Long -> cast is as Double
      if ((column.dataType.isInstanceOf[IntegerType] || column.dataType.isInstanceOf[LongType])  &&
        !column.name.equals(bagIdColumn))
        df = df.withColumn(column.name, df.col(column.name).cast(DoubleType))

      // If the column is StringType -> Index it
      if (column.dataType.isInstanceOf[org.apache.spark.sql.types.StringType]) {
        val indexer = new StringIndexer()
          .setInputCol(column.name)
          .setOutputCol(column.name + "Indexed")

        // Save the new indexed DataFrame
        // Might affect the for loop due to the schema change
        df = indexer
          .fit(df)
          .transform(df)
          .drop(column.name)
      }

    }

    // Return the transformed df
    df
  }


  /**
    * Prints the APR passed as parameter
    *
    * @param apr the APR which will be printed
    */
  def displayAPR(apr : Map[String, (Double, Double)]) : Unit = {

    apr.foreach(x =>{

      println("The bounds of feature " + x._1 + " are: [" + x._2._1 + ", " + x._2._2 + "]")

    })

  }

  /**
    * Writes an APR to a file specified via a path
    *
    * @param apr the APR to be writter
    * @param path the path where the file is located/will be created
    */
  def writeAPRToFile(apr : Map[String, (Double, Double)],
                     path: String) : Unit = {

    val writer = new PrintWriter(new File(path))

    writer.println("NAME,LOWER,UPPER")

    apr.foreach(x =>{

      writer.println(x._1 + "," + x._2._1 + "," + x._2._2)

    })

    writer.close()
  }

  /**
    * Reads the relevant features as specified in a file
    *
    * @param path the path to the file specifying the relevant features
    * @return an array containing the relevant features as specified in the file
    */
  def parseRelevantFeaturesFile(path : String) : Array[String] = {

    var relevantFeatures : Array[String] = Array[String]()

    for(line <- Source.fromFile(path).getLines())
      relevantFeatures = relevantFeatures :+ line

    // Return the relevant features
    relevantFeatures
  }

}
