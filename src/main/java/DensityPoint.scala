package main.java
import org.apache.commons.math3.analysis.{MultivariateFunction, UnivariateFunction}
import org.apache.commons.math3.optim.{InitialGuess, MaxEval, SimpleBounds}
import org.apache.commons.math3.optim.nonlinear.scalar.{GoalType, ObjectiveFunction}
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.BOBYQAOptimizer
import vegas.spec.Spec.Axis
import vegas.spec.Spec.MarkEnums.Line
import vegas.{Axis, Circle, Nominal, Quantitative, Scale, Vegas}

/**
  * Created by razvan on 20.03.2017.
  */
object DensityPoint {


  def distance(p1: Array[Double], p2: Array[Double]): Double =
  {
    p1.zip(p2).map(
      (f) =>
      {
        Math.pow(f._2 - f._1, 2)
      }
    ).sum
  }


  def cost(distance: Double): Double =
  {
    //Math.exp(-distance)
    1/(50+distance)
  }





  def main(args: Array[String]): Unit = {
    val points = Array[Array[Double]] (
      Array[Double](1,1),
      Array[Double](1,1.5),
      Array[Double](1,2),
      Array[Double](2,1),
      Array[Double](15,15),
      Array[Double](4,15),
      Array[Double](13,13),
      Array[Double](14,14)
    )


    val f = new MultivariateFunction {
      override def value(doubles: Array[Double]) = {
        points.map(p=>
          cost(distance(doubles, p))
        ).sum
      }
    }


    val graph = (0.0 to 20.0 by 0.5).flatMap(x =>
      (0.0 to 20.0 by 0.5).map(y =>

        Map[String, Double]("X" -> x, "Y" -> y, "V" -> f.value(Array[Double](x, y)))

      ))

    val plot = Vegas("DensityPoint", width=500.0, height=500.0).
      withData(
        graph
      ).
      encodeX("X", Quantitative).
      encodeY("Y", Quantitative).
      encodeColor("V", Quantitative).
      mark(Circle)

    plot.show

    val optimizer = new BOBYQAOptimizer(2+2)


    val optim = optimizer.optimize(new MaxEval(1000), new ObjectiveFunction(f), GoalType.MAXIMIZE, new InitialGuess(Array[Double](50,50)), new SimpleBounds(Array[Double](-100,-100),Array[Double](200,200)))

    optim.getPoint.foreach(println)

    println(f.value(Array[Double](50,50)))

  }
}