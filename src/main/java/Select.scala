package main.java


import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql
import org.apache.spark.sql._
import org.apache.spark.sql.functions.{array, udf}
import org.apache.spark.sql.types._

/**
  * Created by dan on 19.03.2017.
  */
object Select {

  def main(args : Array[String]) : Unit = {

    val spark = SparkSession
      .builder()
      .appName("SparkSessionZipsExample")
      .getOrCreate()

    import spark.implicits._

    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)

    val APR = Map[String, (Double, Double)](
      "X"-> (5.0, 10.0),
      "Y"-> (2.0,4.0)

    )

    val df = Seq[(Int, Double, Double)](
      (1, 7.0, 2.5),
      (1, 8.0, 2.5),
      (1, 9.0, 2.5),
      (1, 10.25, 2.5),
      (2, 8.0, 6.5),
      (2, 9.0, 5.5),
      (2, 10.25, 4.0),
      (2, 11.0, 3.75),
      (3, 6.0, 6.75),
      (3, 6.0, 5.75),
      (3, 6.0, 4.1),
      (3, 6.0, 3.75)

    ).toDF("BAG_ID", "X", "Y")

    val relevantFeatures : Array[String] = Array[String]("X", "Y")

    featureSelection(df, APR, relevantFeatures, 0.1, "BAG_ID", spark)


    //featureSelection(df, apr, relevantFeatures, 0.05, "BAG_ID", spark)

    //discriminatoryFeaturesPerInstance(df, Array[String]("FEATURE0", "FEATURE1"), apr).show

    /*var bagIds = df.select("BAG_ID").distinct().rdd.map(_.getInt(0)).collect()

    discriminatoryFeaturesPerBag(df, apr, bagIds, "BAG_ID", spark).show()

    /*
    bagsIds.foreach(x => {
      val bagId = x.getInt(0)

      println("The bag Id is: " + bagId)


      df.filter(y => {
        if (y.getAs[Integer]("BAG_ID") == 8)
          true
        else
          false
      }).show()
    })
    */*/



  }

  def featureSelection(dff : DataFrame,
                       apr : Map[String, (Double, Double)],
                       oldRelevantFeatures : Array[String],
                       limit : Double,
                       bagIdColumnName : String,
                       spark : SparkSession) : Array[String] = {

    // Save to var so as to change it later, and filter out those instances which are within the current APR.
    // If a bag is entirely within the APR it will be removed. Some bags will only be partially removed ->
    // this should increase efficiency since those instances will not be processed anymore (they cannot host
    // the max out of bounds feature for a bag, since the instance is completely within the bag -> processing
    // them would be wasteful)
    var df = dff.filter(x => {
      var isOut = false

      // Check if the current instance fits within the bounds of the APR
      for (feature <- apr if !isOut) {

        // Get the feature value
        val value : Double = x.getAs[Double](feature._1)

        // Check the bounds
        if (value < feature._2._1 || value > feature._2._2)
          isOut = true
      }

      // Return the answer
      isOut
    })

    // Only get the bag IDs once, avoid performing too many collects
    var bagIds = df.select(bagIdColumnName).distinct().rdd.map(_.getInt(0)).collect()

    // Store the old Relevant Features in a var so as to allow them to be removed
    var relevantFeatures = oldRelevantFeatures

    // Vector of new Relevant Features
    var newRelevantFeatures : Array[String] = Array[String]()


    // Get the augmented DF, with maximal out of bounds feature
    var maxOutOfBoundsDF = aggregateDF(
      discriminatoryFeaturesPerBag(df, apr, bagIds, bagIdColumnName, spark),
      bagIdColumnName,
      "BEST_BOUND"
    )

    maxOutOfBoundsDF.show



    // Begin the while loop
    while(!relevantFeatures.isEmpty && df.count() > 0) {


      // Get a map of the String -> Discriminated Instances (as DF)
      val mapOfDiscriminations = filterDiscriminatedInstances(maxOutOfBoundsDF, relevantFeatures, apr, limit)

      // Start the process of removing the most discriminating feature
      var maxCount : Long = 0L
      var featureName = ""

      // Find most discriminating feature
      for (entry <- mapOfDiscriminations) {
        val entryCount = entry._2.distinct().count()

        if (entryCount > maxCount) {
          maxCount = entryCount
          featureName = entry._1
        }
      }


      for (i <- mapOfDiscriminations)
      {
        println(i._1)
        i._2.show
      }

      println("ALES: " + featureName)


      maxOutOfBoundsDF = maxOutOfBoundsDF.except(mapOfDiscriminations(featureName))

     /* // Remove its instances from the DF
      df = df.except(mapOfDiscriminations(featureName).drop("BEST_BOUND"))
      //df.show()*/

      // Remove the feature from the old relevant features, so as to avoid it being retested
      relevantFeatures = relevantFeatures.filter(!_.equals(featureName))

      // Add the extracted feature to the relevant features
      newRelevantFeatures = newRelevantFeatures :+ featureName
    }

    // Return the relevant features
    newRelevantFeatures
  }

  def filterDiscriminatedInstances(df: DataFrame,
                                   relFeatures: Array[String],
                                   APR: Map[String, (Double, Double)],
                                   limit: Double) : Map[String, DataFrame] = {

    // featureVal - the value of the feature being tested
    // testedFeature - the name of the feature being tested
    // bestBoundDifferenceR - column in the instance which stores a tuple (will be cast as a row)
    val filterFunc = (featureVal: Row, testedFeature: String, bestBoundDifferenceR: Row) => {

      // Break the row in a tuple (BestFeatureName, Percentage)
      val bestBoundDifference: Tuple2[String, Double] = (bestBoundDifferenceR.getAs[String](0), bestBoundDifferenceR.getAs[Double](1))

      val featureValT = (featureVal.getAs[Double](0), featureVal.getAs[Double](1))

      // Will store the bound difference
      var boundDifference = 0.0

      // Get the bounds of the feature from the APR
      val bound = APR.get(testedFeature).head

      // Compute the difference against the bound
      if (featureValT._1 < bound._1)
        boundDifference = bound._1 - featureValT._1
      else if (featureValT._2 > bound._2)
        boundDifference = featureValT._2 - bound._2

      // Needs percentage computation
      val percentage : Double = getPercentageOutOfBounds(boundDifference, bound._1, bound._2)

      // Return true of false, based on the below boolean expression
      percentage >= limit || (bestBoundDifference._2 < limit && bestBoundDifference._1.equals(testedFeature))
    }

    // Maps between the feature name, and the instances they discriminate against
    var map = Map[String, DataFrame]()

    // Iterate through each feature
    for (relFeature <- relFeatures) {

      // Define a wrapper function which will call filterFunc
      val udfFilterFunc = udf((featureVal: Row, bestBoundDifference: Row) => filterFunc(featureVal, relFeature, bestBoundDifference))

      // Apply the wrapper function on the df iteratively, in order to obtain the filtered DF for each of the features
      map += (relFeature -> df.filter(udfFilterFunc(df(relFeature), df("BEST_BOUND"))))
    }

    // Return the Map
    map
  }

  /**
    * Perform an aggregation which will adapt the DF obtained from
    * the discriminatoryFeaturesPerBag to the filterDiscriminatedInstances
    * requirements
    *
    * @param df the DF that will be aggregated
    * @param bagIdColumnName the name of the column hosting the name of the bag id
    * @param bestBoundColumnName the name of the column hosting the information regarding the most discriminating feature for a bag
    * @return a new aggregated DF
    */
  def aggregateDF(df : DataFrame,
                  bagIdColumnName : String,
                  bestBoundColumnName : String) : DataFrame = {




    val minMaxAgg = new MinMaxAgg()

    val aggCol = df
      .columns
      .filter(x => !x.equals(bagIdColumnName) && !x.equals(bestBoundColumnName))
      .map(x=>minMaxAgg(df(x)).alias(x))

    val fAggCol = aggCol :+ functions.first(bestBoundColumnName).alias(bestBoundColumnName)

    df
      .groupBy(bagIdColumnName)
      .agg(fAggCol.head, fAggCol.tail : _*)
  }

  /**
    * Returns a new DF with a new column for each instance, specifying the maximal feature and out of bounds percentage for the bag
    * that instance belongs to
    *
    * @param df the DF to be processed, containing a number of bags
    * @param apr the APR
    * @param bagIds vector containing the IDs of the bags in the DF
    * @param bagIdColumnName the name of the column hosting the unique bag identifier
    * @param spark the SparkSession
    * @return a new DF containing a new column for each instance, specifying the maximal feature and out of bounds percentage for the bag
    *         that instance belongs to
    */
  def discriminatoryFeaturesPerBag(df : DataFrame,
                                   apr : Map[String, (Double, Double)],
                                   bagIds : Array[Int],
                                   bagIdColumnName : String,
                                   spark : SparkSession) : DataFrame = {


    // Declare the new DF with StructType as new column
   /* var newDf = spark.createDataFrame(
      spark.sparkContext.emptyRDD[Row],
      df.schema.add(StructField("BEST_BOUND",
        StructType.apply(Seq[StructField](StructField("_1", StringType), StructField("_2", DoubleType))))))//.cache()
*/
    /*
    // Doesn't work because Spark doesn't allow for nested RDD processing, i.e. in this case df will throw a NullPointerException
    val bagsIds = df.select(bagIdColumnName).distinct().rdd.map(_.getInt(0))

    bagsIds.foreach(bagId => {

      perBagMaxFeature(df.filter(y => {
        if (y.getAs[Integer]("BAG_ID") == bagId)
          true
        else
          false
      }), apr).show()

      System.exit(1)

    })
    */


    val newDf = bagIds.map( bagID => {
      perBagMaxFeature(df.filter(y => {
        if (y.getAs[Integer]("BAG_ID") == bagID)
          true
        else
          false
      }), apr)
    }).reduce(_.union(_))

    /*bagIds.foreach(bagId => {

      newDf = newDf.union(perBagMaxFeature(df.filter(y => {
        if (y.getAs[Integer]("BAG_ID") == bagId)
          true
        else
          false
      }), apr))

    })*/

    // Return the new DF
    newDf
  }

  /**
    * Receives a bag in a DF, and adds a new column with the maximal feature and maximal out of bounds percentage for the entire bag
    *
    * @param df the DF containing the bag
    * @param apr the all-negative APR
    * @return the DF containing all a new column with the maximal feature and maximal out of bounds percentage for the entire bag
    */
  def perBagMaxFeature(df : DataFrame,
                       apr : Map[String, (Double, Double)]) : DataFrame = {

    var maxPercentage = 0.0
    var maxFeature : String = ""

    // Get the max and min of all features
    val dfMax = df.groupBy().max(apr.keySet.toArray : _*).first()
    val dfMin = df.groupBy().min(apr.keySet.toArray : _*).first()

    for (feature <- apr) {

      /*
      // Gets the max value of a column
      val maxFunction = functions.max(df(feature._1))
      // Gets the min value of a column
      val minFunction = functions.min(df(feature._1))


      // Perform the aggregated functions
      val result = df.agg(minFunction, maxFunction).first()
      */

      // Get the actual values
      val min = dfMin.getAs[Double]("min(" + feature._1 + ")")
      val max = dfMax.getAs[Double]("max(" + feature._1 + ")")

      // Compute value out of bounds
      val outOfBoundsMin : Double = if (min < feature._2._1) feature._2._1 - min else 0.0
      val outOfBoundsMax : Double = if (max > feature._2._2) max - feature._2._2 else 0.0

      val percentageOut : Double = getPercentageOutOfBounds(
        if (outOfBoundsMax > outOfBoundsMin) outOfBoundsMax else outOfBoundsMin,
        feature._2._1,
        feature._2._2)

      /*
      if (min < feature._2._1)
        outOfBoundsMin = feature._2._1 - min

      if (max > feature._2._2)
        outOfBoundsMax = max - feature._2._2

      */
      /*
      // Determine which is the greater out of bounds value, and get its percentage
      if (outOfBoundsMax > outOfBoundsMin)
        percentageOut = getPercentageOutOfBounds(outOfBoundsMax, feature._2._1, feature._2._2)
      else
        percentageOut = getPercentageOutOfBounds(outOfBoundsMin, feature._2._1, feature._2._2)
       */


      // Update the maxFeature and maxPercentage if a better result is found
      if (maxPercentage < percentageOut) {
        maxPercentage = percentageOut
        maxFeature = feature._1
      }

      //println("The min and max are: " + result.getDouble(0) + " " + result.getDouble(1) + " feature " + feature)
    }

    // Define the function which adds a new column
    val addMaxFeatureUdf = udf(() => {
      (maxFeature, maxPercentage)
    })

    // Return the new DF, with the added column
    df.withColumn("BEST_BOUND", addMaxFeatureUdf())
  }

  /**
    * Computes the Out of Bounds Percentage for a feature
    *
    * @param outOfBoundsValue the out of bounds value of the feature
    * @param lowerBound the lower bound of the feature (as specified by the APR)
    * @param upperBound the upper bound of the feature (as specified by the APR)
    * @return the Out of Bounds Percentage
    */
  def getPercentageOutOfBounds(outOfBoundsValue : Double,
                               lowerBound : Double,
                               upperBound : Double) : Double = {

    val range = Math.abs(upperBound - lowerBound)

    //println("Range is: " + range + " out of BoundValue")

    // return the Out of Bounds Percentage
    outOfBoundsValue / range
  }

}
