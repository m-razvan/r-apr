package main.java

import java.io.{File, PrintWriter}

import breeze.integrate._
import org.apache.commons.math3.analysis.UnivariateFunction
import org.apache.commons.math3.analysis.solvers.BrentSolver
import org.apache.commons.math3.distribution.NormalDistribution
import org.apache.spark.mllib.stat.KernelDensity
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.math._

/**
  * Created by dan on 14.03.2017.
  */
object MarginExpand {


  def main(args : Array[String]) : Unit = {
    val spark = SparkSession
      .builder()
      .appName("SparkSessionZipsExample")
      .getOrCreate()

    /*val df = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema",  "true")
      .csv("file:///home/user/Downloads/outOfBounds.csv")

    df.show()*/

    /*
    // Testing
    println("Testing begins: ")

    // Test the Gaussian
    println("The value of the Gaussian is: " + gaussian(0)(1)(0))
    println("The value of the Gaussian is: " + gaussian(0)(1)(2))
    println("The value of the Gaussian is: " + gaussian(0)(1)(3))
    println("The value of the Gaussian is: " + gaussian(0)(1)(100))

    // Test the integral
    val mean = 0.0
    val deviation = 1.0

    val lowerBound = -2.0 * deviation
    val upperBound = 2.0 * deviation

    val accuracy = 20

    println("The integral is: " + simpson(gaussian(mean)(deviation * deviation), lowerBound, upperBound, accuracy))

    */

    /* expandMargins(Map[String, (Double, Double)](
       "FEATURE0" -> (0.0, 4.0),
       "FEATURE1" -> (2.0, 8.0)
     ), df)*/


    val df = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema",  "true")
      .csv("file:///home/razvan/data/APR/expandMargin/expandMargin")

    //     df.show()

    import spark.implicits._

    val APR = Map[String,(Double, Double)]("X"->(10.0,20.0), "Y"->(100.0,200.0))

    println(expandMargins(APR,df).toString())


    /*val kde = approximateKDE(df
      .select(df.col("X"))
      .where(df.col("X").between(10,20))
      .rdd
      .map(x => x.getDouble(0)),
      math.sqrt(findVariance(10,20,0.98)))*/

    /*val seqDF = (0 to 30).map(
      i=>(i.toDouble, kde.estimate(Array[Double](i.toDouble))(0))
    ).toDF("x", "pdf")*/

    /*seqDF.show

    val plot = Vegas("APR", width=500.0, height=500.0).
      withDataFrame(seqDF).


      encodeX("x", Quantitative).
      encodeY("pdf", Quantitative).

      mark(Line)


    plot.show*/


    //println( simpson(x => kde.estimate(Array[Double](x))(0), 5, 25, 100) )



  }


  def findRoot(f: (Double) => Double, min: Double, max:Double) = {
    val brentSolver = new BrentSolver(1E-10)

    val function = new UnivariateFunction {
      override def value(v: Double) = {
        f(v)
      }
    }

    brentSolver.solve(100, function, min, max)
  }


  def findVariance(lowerBound: Double, upperBound: Double, TAU: Double) : Double = {

    //val TAU = 0.98

    def f (x: Double) : Double = {
      val normalDistribution = new NormalDistribution((lowerBound + upperBound) / 2, x)

      normalDistribution.probability(lowerBound, upperBound) - TAU
    }

    val stdDeviation = findRoot(f,1E-3, upperBound - lowerBound)

    math.pow(stdDeviation,2)
  }



  /**
    * Expand the APR
    *
    * @param apr the APR to be expanded
    * @param df the DataFrame of positive bags
    * @return the expanded APR
    */
  def expandMargins(apr : Map[String, (Double, Double)], df : DataFrame) : Map[String, (Double, Double)] = {


    var newAPR = apr

    for (feature <- apr) {


      val lowerBound = feature._2._1
      val upperBound = feature._2._2

      val TAU = 0.98
      val EPS = 0.02
      //val INTEGRATION_STEP = 0.1

      //val INTEGRAL_ACCURACY : Integer = ((upperBound-lowerBound) / INTEGRATION_STEP).toInt
      val INTEGRAL_ACCURACY = 100

      val variance = findVariance(lowerBound, upperBound, TAU)

      val filteredRDD = df
        .select(df.col(feature._1))
        .where(df.col(feature._1).between(feature._2._1, feature._2._2))
        .rdd
        .map(x => x.getDouble(0))

      if (filteredRDD.count()==0)
        return null

      val kde = approximateKDE(filteredRDD,
        math.sqrt(variance))

      val LOWER_LIMIT = lowerBound - 5*(upperBound - lowerBound)
      val UPPER_LIMIT = upperBound + 5*(upperBound - lowerBound)

      val lowerBoundFunction = (upperBoundVar: Double) => simpson(x => kde.estimate(Array[Double](x))(0), LOWER_LIMIT, upperBoundVar, INTEGRAL_ACCURACY) - EPS / 2
      val upperBoundFunction = (lowerBoundVar: Double) => simpson(x => kde.estimate(Array[Double](x))(0), lowerBoundVar, UPPER_LIMIT, INTEGRAL_ACCURACY) - EPS / 2

      val newLowerBound = min(lowerBound, findRoot(lowerBoundFunction, lowerBound  - 5.0 * (upperBound  - lowerBound), (upperBound + lowerBound)/2))
      val newUpperBound = max(upperBound, findRoot(upperBoundFunction, (upperBound + lowerBound)/2, upperBound + 5.0 * (upperBound - lowerBound)))

      newAPR += feature._1 -> (newLowerBound, newUpperBound)

    }

    // return the 'expanded apr'
    newAPR
  }


  /**
    * Get the KDE
    *
    * @param samplePoints the sample points which will be used by the KernelDensity
    * @param stdDeviation the standard deviation
    * @return a KernelDensity which will be used to estimate probabilities for various values
    */
  def approximateKDE(samplePoints : RDD[Double], stdDeviation : Double) : KernelDensity = {

    // Return the KDE
    new KernelDensity().setSample(samplePoints).setBandwidth(stdDeviation)
  }

  /**
    * Compute the value of the Gaussian for x. Curried so as to make it compatible to the signature required by Breeze
    *
    * @param mean the mean value of the Gaussian
    * @param variance the variance of the Gaussian
    * @param x the value of the random variable
    * @return the value of the Gaussian for the value of x
    */
  def gaussian(mean : Double)(variance : Double)(x : Double) : Double = {

    val aux = x - mean

    // Return the value of the Gaussian
    1.0 / sqrt(2.0 * variance * Pi) * exp(-(aux * aux) / (2.0 * variance))
  }

}
