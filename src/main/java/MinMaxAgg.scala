package main.java

import java.io.{File, FileOutputStream, PrintWriter}
import javax.imageio.stream.FileImageOutputStream

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.{Column, DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions.{array, lit, udf}
import org.apache.spark.sql.types.{DoubleType, NumericType}
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._

class MinMaxAgg extends UserDefinedAggregateFunction {

  // Input Data Type Schema
  def inputSchema: StructType = StructType(Array(StructField("feature", DoubleType)))

  // Intermediate Schema
  def bufferSchema = StructType(
    Array(
      StructField("Min", DoubleType),
      StructField("Max", DoubleType)
    )
  )


  /* // Returned Data Type .
   def dataType: DataType = StructType(Array(
     StructField("MinMax", StructType(
       Array(
         StructField("Min", DoubleType),
         StructField("Max", DoubleType)
       )
     ))
   ))*/

  // Returned Data Type .
  def dataType: DataType = StructType(
    Array(
      StructField("Min", DoubleType),
      StructField("Max", DoubleType)
    )
  )


  // Self-explaining
  def deterministic = true

  // This function is called whenever key changes
  def initialize(buffer: MutableAggregationBuffer) = {
    buffer(0) = Double.MaxValue// set sum to zero
    buffer(1) = Double.MinValue // set number of items to 0
  }

  // Iterate over each entry of a group
  def update(buffer: MutableAggregationBuffer, input: Row) = {
    buffer(0) = Math.min(buffer.getDouble(0), input.getDouble(0))
    buffer(1) = Math.max(buffer.getDouble(1), input.getDouble(0))
  }

  // Merge two partial aggregates
  def merge(buffer1: MutableAggregationBuffer, buffer2: Row) = {
    buffer1(0) = Math.min(buffer1.getDouble(0),buffer2.getDouble(0))
    buffer1(1) = Math.max(buffer1.getDouble(1),buffer2.getDouble(1))
  }

  // Called after all the entries are exhausted.
  def evaluate(buffer: Row) = {
    Row(buffer(0), buffer(1))
  }

}