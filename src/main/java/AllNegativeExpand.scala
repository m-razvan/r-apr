package main.java

import org.apache.spark.sql.{DataFrame, SparkSession, functions}
import org.apache.spark.sql.functions._

/**
  * Created by dan on 18.03.2017.
  */
object AllNegativeExpand {

  //var fieldIndexMap : Map[String, Integer] = Map[String, Integer]()

  def main(args : Array[String]) : Unit = {
    val spark = SparkSession
      .builder()
      .appName("SparkSessionZipsExample")
      .getOrCreate()

    import spark.implicits._

    // The two DataFrames must have the same headers!
    val df = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema",  "true")
      .csv("file:///home/dan/IdeaProjects/Backfitting/data/BACKUP/ArtificialSet_2/Extended/ArtificialSetExtended.csv")

    val apr = generateAllNegativeAPR(df, Array[String]("FEATURE1", "FEATURE0"))

    for (feature <- apr)
      println("Feature: " + feature._1 + " [" + feature._2._1 + ", " + feature._2._2 + "]")
  }


  /**
    * Create the apr which should contain most of the negative examples
    *
    * @param dfNegatives the DF containing the negative bags
    * @param dfPositives the DF containing the positive bags
    * @param relevantFeatures the array containing the relevant features
    * @param bagIdColumnName the name of the column hosting the bag identifier
    * @return the APR
    */
  def createAPR(dfNegatives : DataFrame,
                dfPositives : DataFrame,
                relevantFeatures : Array[String],
                bagIdColumnName : String) : Map[String, (Double, Double)] = {

    val initialAPR = generateAllNegativeAPR(dfNegatives, relevantFeatures)

    val extendedAPR = expandAPRFromPositiveBags(initialAPR, dfPositives, relevantFeatures, bagIdColumnName)

    extendedAPR
  }


  /**
    * Expands the APR based on the negative instances of the positive bags
    *
    * @param initialAPR the initial APR which will be expanded
    * @param dfPositives the DF containing the positive bags
    * @param relevantFeatures the relevant features vector
    * @param bagIdColumnName the name of the column hosting the bag id
    * @return the expanded APR
    */
  def expandAPRFromPositiveBags(initialAPR: Map[String, (Double, Double)],
                                dfPositives: DataFrame,
                                relevantFeatures : Array[String],
                                bagIdColumnName : String) : Map[String, (Double, Double)] = {

    // Expand and return the APR - Semi-Distributed
    //HighDensityInstanceExpander.expandAPR(dfPositives, initialAPR, relevantFeatures, bagIdColumnName)

    // Expand and return the APR - Fully-Distributed
    HighDensityInstanceExpanderDistributed.expandAPR(dfPositives, initialAPR, relevantFeatures, bagIdColumnName)
  }


  /**
    * Generate the APR which includes all instances of the negative bags, i.e. the components which have no error
    *
    * @param df the DF containing the negative bags
    * @param relevantFeatures the array containing the relevant features
    * @return the all-negative APR
    */
  def generateAllNegativeAPR(df : DataFrame,
                             relevantFeatures : Array[String]) : Map[String, (Double, Double)] = {

    // Declare the APR
    var apr : Map[String, (Double, Double)] = Map[String, (Double, Double)]()

    // Best
    val dfMax = df.groupBy().max(relevantFeatures : _*).first()
    val dfMin = df.groupBy().min(relevantFeatures : _*).first()

    // The order of the features (in the relevant feature vector) will be consistent with that of the max and min DF
    for (i <- relevantFeatures.indices) {
      apr += (relevantFeatures(i) -> (dfMin.getDouble(i), dfMax.getDouble(i)))
    }


    /*
    // 2nd best
    for (feature <- relevantFeatures) {

      val max = functions.max(df.col(feature))
      val min = functions.min(df.col(feature))

      //val first = functions.first(df.col(feature))
      //val last = functions.last(df.col(feature))

      //println("Whatever: " + df.agg(min, max).first())
      //println("Whatever: " + df.agg(max, min).first())

      val result = df.agg(min, max).first()

      apr += (feature -> (result.getDouble(0), result.getDouble(1)))
    }
    */


    /*
    //3rd best
    // Initiate the APR
    for (feature <- relevantFeatures) {
      apr += (feature -> (Double.MaxValue, Double.MinValue))
    }


    // Generate the APR
    df.collect().foreach(x => {

      for (feature <- apr) {
        val rowFeatureVal = x.getAs[Double](feature._1)

        var min = feature._2._1
        var max = feature._2._2

        if (rowFeatureVal < min)
          min = rowFeatureVal

        if (rowFeatureVal > max)
          max = rowFeatureVal

        if (!feature._2.equals((min, max)))
          apr = apr + (feature._1 -> (min, max))
      }

    })
    */


    // Return the APR
    apr

  }

}
