package main.java

import org.apache.spark.sql.types.{ArrayType, DoubleType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row}

import scala.collection.mutable

/**
  * Created by dan on 27.03.2017.
  */
object HighDensityInstanceExpanderDistributed {

  /**
    * Expands an APR passed as parameter, based on the 'positive' bags, which are considered to have
    * a certain number of negative instances not yet included in the APR
    *
    * @param df the DF containing the positive bags
    * @param apr the APR whose boundaries need expanding
    * @param relevantFeatures the relevant features
    * @param bagIdColName the name of the column hosting the Bag Id
    * @return a new APR with its bounds expanded
    */
  def expandAPR(df : DataFrame,
                apr : Map[String, (Double, Double)],
                relevantFeatures : Array[String],
                bagIdColName : String) : Map[String, (Double, Double)] = {

    // Make a copy of the initial APR, which will be modified and returned at the end of this method
    var APR : Map[String, (Double, Double)] = apr

    val bagAggregate : AggregateBag = new AggregateBag(df.schema, relevantFeatures)

    val allColumns = df.columns.map(colName => df(colName))

    // Aggregate by bag - remove the irrelevant features
    val temp = df
      .groupBy(df.col(bagIdColName))
      .agg(bagAggregate(allColumns : _*).alias("TEMP"))

    // Create a new Density Aggregator
    val hdp : HighestDensityPointAgg = new HighestDensityPointAgg(
      relevantFeatures,
      valueFunction(0.5),
      // Pass the same schema as for the AggregateBag
      includePoint(
        apr,
        relevantFeatures,
        10000000000000000.0,
        0.0
      )
    )

    // Get the array of rows containing the (Min, Max) tuples from the 'positive' bags
    val results = temp
      .groupBy()
      .agg(hdp(temp.col("TEMP")))
      .first().getAs[mutable.WrappedArray[Row]](0)

    // Iterate through the relevant features, and adjust the APR where need be
    for (i <- relevantFeatures.indices) {

      val resultTuple : (Double, Double) = (results(i).getAs[Double](0), results(i).getAs[Double](1))

      val currentBounds = apr(relevantFeatures(i))

      APR += relevantFeatures(i) -> (
        if (resultTuple._1 < currentBounds._1) resultTuple._1 else currentBounds._1,
        if (resultTuple._2 > currentBounds._2) resultTuple._2 else currentBounds._2
      )

    }

    // Return the APR
    APR
  }


  /**
    * Predicate which decides if a certain point is truly a high density negative instance
    *
    * @param APR the APR against which the distance will be computed
    * @param relevantFeatures the array hosting the relevant features
    * @param point the point itself
    * @param density the density of the point
    * @return true if the point is a high density negative instance, false otherwise
    */
  private def includePoint(APR: Map[String, (Double, Double)],
                           relevantFeatures : Array[String],
                           maxDistance : Double,
                           minDensity : Double)
                          (point: Seq[Double],
                           density: Double) : Boolean = {

    // Compute the Manhattan distance
    val distance = APR.map { case (feature: String, (lowerBound: Double, upperBound: Double)) =>

      val pointValue = point(relevantFeatures.indexOf(feature))

      if (pointValue < lowerBound)
        lowerBound - pointValue
      else if (pointValue > upperBound)
        pointValue - upperBound
      else
        0
    }.sum

    if (distance <= maxDistance && density >= minDensity)
      true
    else
      false
  }

  /**
    * Computes the value of a point in relation to the distance with another point
    *
    * @param eps the planar displacement
    * @param pointOne the first point
    * @param pointTwo the second point
    * @return the value of either of the points
    */
  private def valueFunction(eps : Double)
                   (pointOne : Seq[Double],
                    pointTwo : Seq[Double]) : Double = {

    // Compute the Euclidean distance
    val distance = pointOne.zip(pointTwo).map(
      f => {
        Math.pow(f._2 - f._1, 2)
      }
    ).sum

    // Return the value
    1 / (eps + distance)
  }


}
