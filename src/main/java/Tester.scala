package main.java

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{SparkSession}

/**
  * Created by dan on 27.03.2017.
  */
object Tester {

  def main (args: Array[String]) : Unit = {
    val spark = SparkSession
      .builder()
      .appName("SparkSessionZipsExample")
      .getOrCreate()

    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.FATAL)

    // The two DataFrames must have the same headers!
    val df = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema",  "true")
      .csv("/home/dan/IdeaProjects/R-APR/data/Dataset_2/Split/positives.csv")


    val apr : Map[String, (Double, Double)] = Map[String, (Double, Double)] (
      "FEATURE0" -> (22.4, 52.6),
      "FEATURE1" -> (19.7, 50.1)
    )

    val APR = HighDensityInstanceExpanderDistributed.expandAPR(df, apr, Array("FEATURE0", "FEATURE1"), "BAG_ID")

    RAPR.displayAPR(APR)


  }

}
